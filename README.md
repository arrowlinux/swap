# swap

To use it is simple.
There are two ways to run this.

This is the simplest form

1: 
$ ./swap.sh




or to run with swapfile size pre-entered
This is the fastest form. # Numbers only "2, 4, 6, 8, 16" Whatever amount (In GB) you want. You can also use decimals "8.5, 4.7. 12.2" and so on. But do NOT put letters.

2:
$ ./swap.sh 8        # for an 8GB   swapfile.
$ ./swap.sh 8.5      # for an 8.5GB swapfile.



# TIP
# 
You can add an alias to your .bashrc/.zshrc file like this
# 
$ cp ~/.bashrc ~/.bashrc.bak && echo alias swap="/path/to/script/swap.sh" >> ~/.bashrc   # change the path to the swap.sh location
#
Then you only need to enter
#
$ swap 8
#
And that will achieve the same thing
#
This script calls sudo when needed, so there is no need to run as root.
# Enjoy