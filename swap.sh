#!/bin/bash


# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# Created on 29/Jan/2020 ..... Last updated, 17/Jun/2021
#------------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# I wrote this script from scratch
#
###############################################################################
#
#       If you find this script useful
#
#       help keep this project afloat. donate to
#       my Bitcoim wallet at 
#       bc1q0ccha04u5cvvrulvgnmh3u4wfl4wpfxeksqusw
#       
#       
#       paypal.me/eliwal
#######################################################################
####################### NEEDED INFORMATION ############################
#######################################################################
#
# Optionally you can add the amount of Gigabites you want 
# as you call the script,
# like this
#
# /path/to/script/swap.sh 8
#
# That will create a 8 GB swapfile. 
# You can also use "." like 8.5 but do NOT add letters.
# It will fail if you do. 
#
# You also do not need to run it with sudo.
# It calls sudo in the script.
#
# TIP
# 
# You can add an alias to your bashrc/zshrc file
# like this
# 
# alias swap="/path/to/script/swap.sh"
#
# Then you only need to enter
#
# swap 8
#
# And that will achieve the same thing
#
# Enjoy
#
######################################################################
######################################################################
######################################################################
ERR()
{

  echo >&2 "$(tput bold; tput setaf 1)[-] ${*}$(tput sgr0)"
#  exit 86
}

ERR2()
{

  echo >&2 "$(tput bold; tput setaf 1)    ${*}$(tput sgr0)"
#  exit 86
}

# check for root privilege function
check_root()
{
  if [ "$(id -u)" -ne 0 ]; then
    ERR "you must be root" ; exit 86
  else
     msg 'script has root privileges'
  fi
}

# warning message wrapper function
warn()
{
  echo >&2 "$(tput bold; tput setaf 3)[!] ${*}$(tput sgr0)"

}

# warning message wrapper function
warn2()
{
  echo >&2 "$(tput bold; tput setaf 3)    ${*}$(tput sgr0)"

}

# standard message wrapper function
msg()
{
  echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"
}

# standard message wrapper function
msg2()
{
  echo "$(tput bold; tput setaf 2)    ${*}$(tput sgr0)"
}

msg3()
{
  echo "$(tput bold; tput setaf 6)    ${*}$(tput sgr0)"
}
clear ; clear
VAR="$1"
swapfile_check(){

##       grep -q "swapfile" "/etc/fstab"
        grep -q "swap" "/etc/fstab"
     if [ $? -eq 0 ]
        then
 #           clear ; clear
            echo ""
            echo ""
            warn "swapfile is already added to fstab."
            warn2 "No changes will be made to it." 
        else 
            clear ; clear
            echo ""
            msg3 "adding swapfile to fstab...." ; sleep 2
            echo ""
            echo "/swapfile none swap defaults 0 0" >> /etc/fstab
            if [ $? = 0 ]
               then
                   msg "swapfile was added to fstab."
                   msg2 "No errors were encountered." ; sleep 2
               else
                   ERR "swapfile was NOT added to fstab."
                   ERR2 "An error was encountered." ; sleep 2 
            fi
    fi

}

error_check(){

         if [ $? = 0 ]
            then
                echo ""
                msg "swapfile was successfully updated."
                msg2 "No errors were encountered."
                echo ""
            else
                echo ""
                ERR "swapfile was NOT updated!!!"
                ERR2 "Please try again."
                echo ""
         fi
}

myswap1(){

   SwP="${SWP}G"
         echo ""
         echo ""
         msg "You entered ${VAR}"
         msg2 "creating a ${VAR} Gb swapfile"
         echo ""
         sudo swapoff /swapfile
         sudo rm /swapfile
         sudo fallocate -l ${VAR}G /swapfile && sudo chmod 600 /swapfile && sudo mkswap /swapfile && sudo swapon /swapfile
         error_check
}

myswap2(){

   SwP="${SWP}G"
        echo ""
        echo ""
        echo ""
        msg "This is where you enter the"
        msg2 "Swapfile size you want (in GB)"
        msg3 "Numbers only, like 4 or 8.5 or 16 No leters."
        read answer
   case "$answer" in

         $answer) 
               echo ""
               echo ""
               msg "You entered ${answer}"
               msg3 "creating a ${answer} Gb swapfile"
               echo ""
               sudo swapoff /swapfile 
               sudo rm /swapfile
               sudo fallocate -l ${answer}G /swapfile && sudo chmod 600 /swapfile && sudo mkswap /swapfile && sudo swapon /swapfile
               error_check
               ;;
   esac
}

getDate(){
	date
	
	return
}

freee(){
    echo ""
    free -h
    echo ""
    echo ""
}

if [ ! -z $1 ] 
then 
     swapfile_check && myswap1
else
     clear ; clear
     freee && swapfile_check && myswap2
fi


getDate
freee

# end of script

